React Noughts and Crosses game.


Learning and understanding the fundamentals of React JS by completing the tutorial at "https://reactjs.org/"


Game features:

- History of players' moves
- Time travel: to revert back to previous moves
- An indicator for whose turn it is
- A declaration of who won


